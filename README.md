# Angular products API

(Projet en cours)

Consommer l'API products.

Le but :
* Apprendre Angular

Technologies utilisées :
* Angular 14
  * TypeScript

Que fait l'Application ? :
* Possibilité de voir un ou tous les produits

A venir :

* Ajouter/Modier des produits
* Supprimer des produits
* Voir les produits par utilisateurs
* Tests


